# Emoji Picker Project Skeleton

A react app which implements Emoji Picker


**Time taken for this project: 2:31:54**

![] (Time.png)


### Preview

![] (AroundTakeAwayFinal.gif).


### Summary

It was fun making this project. I used Blueprintjs, which has a great variety for styling the components. Also, I got to learn about libraries like react-tether and unicode-emoji-map and how they efficiently helped in solving the problem statement.

Coming to my contribution in project, I added a popover on the button in `EmojiButton` React component which renders on clicking the button. Then, I used `Tabs` component from blueprintjs to render the category of the emojis and then the `Tab` component to render the emojis of the selected category. I also added a `EmojiContainer` React component which is used to render the emojis and on hover over any emoji it displays the name(tooltip) of that emoji. On clicking any emoji, a redux action is dispatched which renders the name of emoji on the screen.


### Challenges Faced

I faced a couple of challenges. Firstly, learning about couple of new libraries like react-tether and unicode-emoji-map. It did take some time. Also, took some time to learn about Bitbucket since I was using it for the first time. Secondly, the styling of the popover took time for me. At last, implementing the functionality within a given time frame was challenging.


### Trade-offs

- Choosing blueprintjs over react-tether to render the popup. Blueprintjs comes with a popover component to render a popover so I chose to go with Blueprintjs itself and not use another library. Honestly, I am not a big fan of adding dependencies in a project until required and so I went ahead using Blueprintjs and not react-tether.





