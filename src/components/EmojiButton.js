import React, { Component } from 'react';
import { Popover, PopoverInteractionKind, Button, Position } from '@blueprintjs/core';
import { connect } from 'react-redux';
import EmojiPicker from './EmojiPicker';

class EmojiButton extends Component {
    constructor(props) {
        super(props);
        this.state = { isOpen: false };

        this.handlePopover = this.handlePopover.bind(this);
    }

    handlePopover() {
        this.setState({ isOpen: !this.state.isOpen });
    }
    render() {
        const { isOpen } = this.state;
        return (
            <div className="emoji-button">
                <Popover
                    canEscapeKeyClose
                    interactionKind={PopoverInteractionKind.CLICK}
                    position={Position.BOTTOM}
                    isOpen={isOpen}
                >
                    <Button onClick={this.handlePopover} text="CLICK ME" intent="primary" />
                    {<EmojiPicker />}
                </Popover>
            </div>
        )
    }
}

export default connect()(EmojiButton);
