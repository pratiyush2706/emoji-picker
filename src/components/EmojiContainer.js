import React from 'react';
import { Tooltip } from '@blueprintjs/core';
import unicodeMap from 'emoji-unicode-map';
import * as actions from '../redux/actions';
import { connect } from 'react-redux';


const EmojiContainer = ({ emojisList, displayEmoji }) => {

  return (
    <div className='popover-tab'>
      {emojisList.map(emoji => (
        <div className="emoji" onClick={() => displayEmoji(emoji)} >
          <Tooltip content={<span> {unicodeMap.get(emoji)} </span>}>
            {emoji}
          </Tooltip>
        </div>
      ))}
    </div>
  )
};

const mapDispatchToProps = dispatch => ({
  displayEmoji: (emoji) => dispatch(actions.setEmoji(emoji))
});


export default connect(null, mapDispatchToProps)(EmojiContainer);