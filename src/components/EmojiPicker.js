import React, { Component } from 'react';
import { Tabs, Tab } from '@blueprintjs/core';

import { emojis, categories } from './emojis';
import EmojiContainer from './EmojiContainer';

class EmojiPicker extends Component {

    state = { currentId: Object.keys(categories)[0] };

    handleTabChange = (newTabId) => {
        this.setState({ currentId: newTabId });
    }
    render() {
        return (
            <Tabs onChange={this.handleTabChange} animate={true} id="people" selectedTabId={this.state.currentId} large={true}>
                {Object.entries(categories).map(([categoryName, logo]) => (
                    <Tab id={categoryName} title={logo} key={categoryName} panel={<EmojiContainer emojisList={emojis[categoryName]} />} />
                ))}
            </Tabs>
        )
    }
}

export default EmojiPicker;